##Game Editor##


###Problems###

The editor can change the operand of the game without disconnecting the clients connected. 

The editor has to be crossplatform.

###Goals###

Develop a tool to edit the the operand of the game.

The game is a continuation of calculus with a specific operand, the player has to submit the right answer.

The operand can be changed by the game designer as he wants.

###User Story###

The player launch a client executable, and can submit his answer to the server.

The game designer can launch the editor tool executable to modify the operand of the game (plus, minus, division, multiply).

###Architecture###

The project is composed of three main parts: client side, server side, and editor tool side.

The client side handle the client exe within a Windows Form Application, to submit the answer to the server.

The server side handle the server exe within a Windows Console Application, and handle the clients connections, 
established the calculus, send the calculus to the client, get and check the client answer, and set the operand of the game setted by the game designer.

The tool side handle the editor exe within a Windows Presentation Foundation, to let the game designer decides the operand of the game as he wants.

###Details of Components###

The server creates a local socket (127.0.0.1), and send the calculus to the clients connected.

The clients answers are checked, and a message is send to them with the right answer. A new calculus is send. 

It handles the operand of the game setted by the game designer. This operand is used to established the calculus.

The client connects to the server, gets the calculus from the server, and an input is available to let the player answer and get the right result.

The editor connects to the server, and four operands choices are available : Add, Sub, Mul, Div. The game designer chooses the operand wanted, and the editor sends this choice to the server.