﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace EditorTool
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        enum Operand
        {
            ADD,
            SUB,
            DIV,
            MUL
        };

        string operand = "Operand send to the server: ";
        string ip = "127.0.0.1";
        int port = 8888;
        Operand myOperand = Operand.ADD;

        System.Net.Sockets.TcpClient toolSocket = new System.Net.Sockets.TcpClient();

        public MainWindow()
        {
            InitializeComponent();
            ConnectionServer();
        }

        public void ConnectionServer()
        {
            toolSocket.Connect(ip, port);
            CheckConnection();
        }

        public void CheckConnection()
        {
            if (toolSocket.Connected == true)
                myLabel.Content = operand + "SUCCESS !";
            else
                myLabel.Content = operand + "FAIL !";
        }

        private void AddToServer(object sender, RoutedEventArgs e)
        {
            this.myOperand = Operand.ADD;
            this.SendOperand("ADD");
        }

        private void SubToServer(object sender, RoutedEventArgs e)
        {
            this.myOperand = Operand.SUB;
            this.SendOperand("SUB");
        }

        private void DivToServer(object sender, RoutedEventArgs e)
        {
            this.myOperand = Operand.DIV;
            this.SendOperand("DIV");
        }

        private void MulToServer(object sender, RoutedEventArgs e)
        {
            this.myOperand = Operand.MUL;
            this.SendOperand("MUL");
        }

        public void SendOperand(string operand)
        {
            //MessageBox.Show("operand= " + operand);

            NetworkStream serverStream = toolSocket.GetStream();
            byte[] outStream = System.Text.Encoding.ASCII.GetBytes(operand);
            serverStream.Write(outStream, 0, outStream.Length);
            serverStream.Flush();

        }
    }
}
