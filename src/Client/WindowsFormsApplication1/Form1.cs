﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        System.Net.Sockets.TcpClient clientSocket = new System.Net.Sockets.TcpClient();
        NetworkStream serverStream;

        public Form1()
        {
            InitializeComponent();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            serverStream.Close();
            e.Cancel = true;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            msg("Client Started");
            clientSocket.Connect("127.0.0.1", 8888);
            //label1.Text = "Client Socket Program - Server Connected ...";

            button1.Text = "PLAY";

            serverStream = clientSocket.GetStream();

            byte[] inStream = new byte[100025];
            serverStream.Read(inStream, 0, (int)clientSocket.ReceiveBufferSize);

            string returndata = System.Text.Encoding.ASCII.GetString(inStream);
            returndata = returndata.Replace("NL", "\r\n >> Server : ");
            msg("Server : " + returndata);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Text = "Submit";
            string result = textBox2.Text;

            if (result.Length == 0)
                result = "0";
            int no;
            if (int.TryParse(result, out no))
            {

                byte[] outStream = System.Text.Encoding.ASCII.GetBytes(result);
                serverStream.Write(outStream, 0, outStream.Length);
                serverStream.Flush();
            }
            else
            {
                byte[] outStream = System.Text.Encoding.ASCII.GetBytes("0");
                serverStream.Write(outStream, 0, outStream.Length);
                serverStream.Flush();
            }
            serverStream = clientSocket.GetStream();

            byte[] inStream = new byte[100025];
            serverStream.Read(inStream, 0, (int)clientSocket.ReceiveBufferSize);

            string returndata = System.Text.Encoding.ASCII.GetString(inStream);
            returndata = returndata.Replace("NL", "\r\n >> Server : ");
            msg("Server : " + returndata);





        }

        public void msg(string mesg)
        {
            textBox1.Text = textBox1.Text + Environment.NewLine + " >> " + mesg;
        }
    }
}
