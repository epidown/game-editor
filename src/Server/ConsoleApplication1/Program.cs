﻿using System;
using System.Threading;
using System.Net.Sockets;
using System.Text;

namespace ConsoleApplication1
{

    public enum Operator
    {
        PLUS,
        MINUS,
        TIME,
        DIVIDE
    };

    public static class MyOperand
    {
       public static Operator operand = Operator.PLUS;
    }


    class Program
    {
        
        static void Main(string[] args)
        {
            TcpListener serverSocket = new TcpListener(8888);
            TcpClient clientSocket = default(TcpClient);
            int counter = 0;

            serverSocket.Start();
            Console.WriteLine(" >> " + "Server Started");

            counter = 0;
            while (true)
            {
                counter += 1;
                clientSocket = serverSocket.AcceptTcpClient();
                Console.WriteLine(" >> " + "Client No:" + Convert.ToString(counter) + " started!");
                handleClinet client = new handleClinet();
                client.startClient(clientSocket, Convert.ToString(counter));
            }

            clientSocket.Close();
            serverSocket.Stop();
            Console.WriteLine(" >> " + "exit");
            Console.ReadLine();
        }
    }


    //Class to handle each client request separatly
    public class handleClinet
    {

        TcpClient clientSocket;
        string clNo;
        public void startClient(TcpClient inClientSocket, string clineNo)
        {
            this.clientSocket = inClientSocket;
            this.clNo = clineNo;
            Thread ctThread = new Thread(doChat);
            ctThread.Start();
        }
        private void doChat()
        {
            int requestCount = 0;
            byte[] bytesFrom = new byte[100025];
            string dataFromClient = null;
            Byte[] sendBytes = null;
            string serverResponse = null;
            string rCount = null;
            requestCount = 0;
            bool firstConnection = true;

            
            string operandChar = "";
            int lastResult = 0;
            // Check First Message

            while ((true))
            {
                try
                {
                    requestCount = requestCount + 1;
                    if (clientSocket.Connected)
                    {
                        NetworkStream networkStream = clientSocket.GetStream();
                        if (!firstConnection)
                        {
                            networkStream.Read(bytesFrom, 0, (int)clientSocket.ReceiveBufferSize);
                            dataFromClient = System.Text.Encoding.ASCII.GetString(bytesFrom);

                            if (dataFromClient.Contains("ADD"))
                                MyOperand.operand = Operator.PLUS;

                            if (dataFromClient.Contains("SUB"))
                                MyOperand.operand = Operator.MINUS;
                            if (dataFromClient.Contains("DIV"))
                                MyOperand.operand = Operator.DIVIDE;
                            if (dataFromClient.Contains("MUL"))
                                MyOperand.operand = Operator.TIME;

                            int x = int.Parse(dataFromClient);
                            if (x == lastResult)
                                serverResponse = "Good Job Bro" + "NL";
                            else
                                serverResponse = "What a fail ... The answer was : " + lastResult + "NL";
                            Console.WriteLine(" >> " + serverResponse);
                           // networkStream.Write(bytesFrom, 0, (int)clientSocket.ReceiveBufferSize);

                        }
                        Console.WriteLine(" >> " + "From client-" + clNo + dataFromClient);


                        rCount = Convert.ToString(requestCount);
                                

                        int rNumber1;
                        int rNumber2;

                        rNumber1 = 0;
                        rNumber2 = 0;
                        rNumber1 = RandomNumber();
                        rNumber2 = RandomNumber();

                        /// <summary>
                        /// TODO Check division
                        /// </summary>


                        if (MyOperand.operand == Operator.PLUS)
                        {
                            operandChar = "+";
                            lastResult = rNumber1 + rNumber2;
                        }
                        else if (MyOperand.operand == Operator.MINUS)
                        {
                            operandChar = "-";
                            lastResult = rNumber1 - rNumber2;
                        }
                        else if (MyOperand.operand == Operator.DIVIDE)
                        {
                            operandChar = "/";
                            lastResult = rNumber1 % rNumber2;
                        }
                        else if (MyOperand.operand == Operator.TIME)
                        {
                            operandChar = "*";
                            lastResult = rNumber1 * rNumber2;
                        }

                        // Modify response // do operations // allocate lastResult
                        serverResponse +=  rNumber1 + operandChar + rNumber2;
                        sendBytes = Encoding.ASCII.GetBytes(serverResponse);
                        networkStream.Write(sendBytes, 0, sendBytes.Length);
                        networkStream.Flush();
                        Console.WriteLine(" >> " + serverResponse);
                        firstConnection = false;
                        sendBytes = null;


                        firstConnection = false;

                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(" >> " + ex.ToString());
                }
            }
        }

        Random random = new Random();

        private int RandomNumber()
        {
            return random.Next(1, 100);
        }


    }
}
